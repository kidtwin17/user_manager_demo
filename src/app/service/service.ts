import { User } from './../model/user';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class Service {
    headers: Headers;
    options: RequestOptions;
    constructor(private http: Http, private httpClient: HttpClient) { 
       this.headers = new Headers({ 'Content-Type': 'application/json'});
       this.options = new RequestOptions({ headers: this.headers });
    }

    storeUser(newUser): Observable<any> {
        return this.http.post('http://localhost:3000/users', newUser, this.options);
   }

   readData(): Observable<User> {
    return this.httpClient.get<User>("http://localhost:3000/users");
  }
}