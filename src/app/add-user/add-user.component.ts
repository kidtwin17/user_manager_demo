import { Service } from './../service/service';
import { User } from './../model/user';
import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  yourModelDate: any 
  user: User = {
    name: '',
    surname: '',
    dob: null,
    age: 0
  };

  dateOfBirth: any;

  submitted = false;

  data:any;

  constructor(private service:Service, private toaster: ToastrService) {   }

  ngOnInit(): void {
  }


  addUser(form: NgForm): void {
    this.submitted = true;

    let formattedDate = (moment(form.form.value.dob)).format('YYYY/MM/DD')
    
    let data = {
      name: form.form.value.name,
      surname: form.form.value.surname,
      dob: formattedDate,
      age: this.compute_age(formattedDate)
    };
    this.service.storeUser(data).subscribe(data => {
      console.log(data);
      this.toaster.show(form.form.value.name + " has been added");
      form.form.value.name = '';
      form.form.value.surname = '';
      form.form.value.dob = null;
      
    }); 

  }
  
  compute_age(dob): number { 

    let day = moment(dob, 'YYYY/MM/DD').date();
    let month = 1 + moment(dob, 'YYYY/MM/DD').month();
    let year = moment(dob, 'YYYY/MM/DD').year();

    var diff_ms = Date.now() - new Date(year, month, day).getTime();
    var age_dt = new Date(diff_ms); 
  
    return Math.abs(age_dt.getUTCFullYear() - 1970);
}

}
