import { Service } from './../service/service';
import { User } from './../model/user';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import * as moment from 'moment';


@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

  data = true;
  average_age = 0;
  title = 'my-app';

  columnDefs = [
      { field: 'name', sortable: true  },
      { field: 'surname' },
      { field: 'dob'},
      { field: 'age'}
  ];

  rowData: any;

  headers: Headers;
  options: RequestOptions;

  constructor(private httpClient: HttpClient, private http: Http, private server: Service) {
    this.headers = new Headers({ 'Content-Type': 'application/json'});
    this.options = new RequestOptions({ headers: this.headers }); 

    this.server.readData().subscribe(data =>{
     
      this.rowData = data;
      
      console.log(this.rowData.length);
      if(this.rowData.length >0)
          this.average_age = this.getAvgAge(data);
    }); ;
   }

  ngOnInit(): void {}
  
  getAvgAge(users): number {

    let getAverage = arr => {
      let reducer = (total, currentValue) => total + currentValue;
      let sum = arr.reduce(reducer)
      return sum / arr.length;
    };

    let ages = users.map(data => data.age);

    return getAverage(ages);
  }

}
