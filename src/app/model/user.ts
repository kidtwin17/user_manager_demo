export interface User {
  name: string;
  surname: string;
  dob: any;
  age: number;
}
