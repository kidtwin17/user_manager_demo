import { ViewUsersComponent } from './view-users/view-users.component';
import { AddUserComponent } from './add-user/add-user.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import {PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AgGridModule } from 'ag-grid-angular';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule} from '@angular/material/core';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { HttpModule } from '@angular/http';
import { Service } from './service/service';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule, 
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatInputModule,
    HttpClientModule,
    HttpModule,
    MatNativeDateModule,

    RouterModule.forRoot([
      { path: 'add-user', component: AddUserComponent },
      { path: 'view-users', component: ViewUsersComponent },
      { path: '', redirectTo: '/add-user', pathMatch: 'full' },
      { path: '**', component: PageNotFoundComponent }
  ],  { relativeLinkResolution: 'legacy' } ),
  AgGridModule.withComponents([]),
  ToastrModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    AddUserComponent,
    ViewUsersComponent
  ],
  providers: [ Service ],
  bootstrap: [ AppComponent ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
